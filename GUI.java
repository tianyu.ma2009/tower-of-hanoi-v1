import java.awt.*;
import javax.swing.*;

public class GUI
{
  public static void main(String[] args)
  {
    JFrame frame = new JFrame("Tower");
    frame.setLayout(null);
    frame.setSize(430, 300);

    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    int width = (dim.width - frame.getWidth()) / 2;
    int height = (dim.height - frame.getHeight()) / 2;
    frame.setLocation(width, height);

    JLabel label1 = new JLabel("A");
    JLabel label2 = new JLabel("B");
    JLabel label3 = new JLabel("C");
    JLabel move = new JLabel("Moves: ");
    JLabel count = new JLabel("0");
    JLabel credit = new JLabel("by Tianyu");

    JButton stick1 = new JButton();
    JButton stick2 = new JButton();
    JButton stick3 = new JButton();

    JButton disk1 = new JButton("1");
    JButton disk2 = new JButton("2");
    JButton disk3 = new JButton("3");
    JButton base = new JButton("Try to shift all the disks under C.");

    int[] labelX = { 100, 210, 320 };
    int[] labelY = { 30, 30, 30 };
    int labelW = 40;
    int labelH = 40;

    int[] stickX = { 100, 210, 320 };
    int[] stickY = { 70, 70, 70 };
    int stickW = 10;
    int stickH = 128;

    int[] diskX = { 75, 65, 55 };
    int[] diskY = { 120, 145, 170 };
    int[] diskW = { 60, 80, 100 };
    int[] diskH = { 30, 30, 30 };

    label1.setBounds(labelX[0], labelY[0], labelW, labelH);
    label2.setBounds(labelX[1], labelY[1], labelW, labelH);
    label3.setBounds(labelX[2], labelY[2], labelW, labelH);

    stick1.setBounds(stickX[0], stickY[0], stickW, stickH);
    stick2.setBounds(stickX[1], stickY[1], stickW, stickH);
    stick3.setBounds(stickX[2], stickY[2], stickW, stickH);

    disk1.setBounds(diskX[0], diskY[0], diskW[0], diskH[0]);
    disk2.setBounds(diskX[1], diskY[1], diskW[1], diskH[1]);
    disk3.setBounds(diskX[2], diskY[2], diskW[2], diskH[2]);

    move.setBounds(10, 0, 50, 30);
    count.setBounds(60, 0, 100, 30);
    base.setBounds(30, 195, 370, 45);
    credit.setBounds(185, 250, 100, 30);

    frame.add(label1);
    frame.add(label2);
    frame.add(label3);
    frame.add(disk1);
    frame.add(disk2);
    frame.add(disk3);
    frame.add(stick1);
    frame.add(stick2);
    frame.add(stick3);
    frame.add(move);
    frame.add(count);
    frame.add(base);
    frame.add(credit);

    stick1.setEnabled(false);
    stick2.setEnabled(false);
    stick3.setEnabled(false);
    base.setEnabled(false);
    credit.setForeground(Color.blue);

    frame.setDefaultCloseOperation(3);
    frame.setResizable(false);
    frame.setVisible(true);

    MyActionListener listener = new MyActionListener(disk1, disk2, disk3, count, base);
    disk1.addActionListener(listener);
    disk2.addActionListener(listener);
    disk3.addActionListener(listener);
  }
}