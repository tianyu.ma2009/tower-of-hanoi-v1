import javax.swing.JButton;
import java.awt.*;
import java.applet.*;

public class Methods extends Applet
{
  public void p(String str)
  {
    System.out.println(str);
  }
  public int getX(JButton current) {
    int X = 0;
    Point o = current.getLocation();

    if (current.getWidth() == 60) X = o.x + current.getWidth() / 2;
    if (current.getWidth() == 80) X = o.x + current.getWidth() / 2;
    if (current.getWidth() == 100) X = o.x + current.getWidth() / 2;

    return X;
  }

  public int getY(JButton current)
  {
    int Y = 0;
    Point o = current.getLocation();

    if (current.getWidth() == 60) Y = o.y + current.getHeight() / 2;
    if (current.getWidth() == 80) Y = o.y + current.getHeight() / 2;
    if (current.getWidth() == 100) Y = o.y + current.getHeight() / 2;

    return Y;
  }

  public boolean canBeMoved(JButton current, JButton b1, JButton rest2)
  {
    Point pCurrent = current.getLocation();
    Point pRest1 = b1.getLocation();
    Point pRest2 = rest2.getLocation();

    int currentX = pCurrent.x + current.getWidth() / 2;
    //int currentY = pCurrent.y;

    int rest1X = pRest1.x + b1.getWidth() / 2;
    //int rest1Y = pRest1.y;

    int rest2X = pRest2.x + rest2.getWidth() / 2;
    //int rest2Y = pRest2.y;

    if (current.getWidth() == 100)
    {
      if ((rest1X == currentX) || (rest2X == currentX) || 
        ((rest1X == currentX) && (rest2X == currentX)) || 
        (currentX + rest1X + rest2X == 635)) return false;

    }
    else if (currentX == rest1X) return false;

    return true;
  }

  public int nStickLocation(JButton current, JButton rest1, JButton rest2)
  {
    Point p = current.getLocation();
    int currentX = p.x;

    int currentStickLocation = currentX + current.getWidth() / 2;
    if (currentStickLocation == 105) return 215;
    if (currentStickLocation == 215) return 325;
    return 105;
  }

  public int nnStickLocation(JButton current, JButton rest1, JButton rest2)
  {
    Point p = current.getLocation();
    int currentX = p.x;

    int currentStickLocation = currentX + current.getWidth() / 2;
    if (currentStickLocation == 105) return 325;
    if (currentStickLocation == 215) return 105;
    return 215;
  }

  public int nStickHasDisks(JButton current, JButton rest1, JButton rest2)
  {
    int nextStickLocation = nStickLocation(current, rest1, rest2);

    p("next stick location: " + nextStickLocation);

    Point p1 = rest1.getLocation();
    int rest1X = p1.x + rest1.getWidth() / 2;
    Point p2 = rest2.getLocation();
    int rest2X = p2.x + rest2.getWidth() / 2;

    int numberOfDisksOnNextStick = 0;
    if (rest1X == nextStickLocation) numberOfDisksOnNextStick++;
    if (rest2X == nextStickLocation) numberOfDisksOnNextStick++;

    p("number of disks on next stick: " + numberOfDisksOnNextStick);

    return numberOfDisksOnNextStick;
  }

  public int nnStickHasDisks(JButton current, JButton rest1, JButton rest2)
  {
    int nextNextStickLocation = nnStickLocation(current, rest1, rest2);

    p("next next stick location: " + nextNextStickLocation);

    Point p1 = rest1.getLocation();
    int rest1X = p1.x + rest1.getWidth() / 2;
    Point p2 = rest2.getLocation();
    int rest2X = p2.x + rest2.getWidth() / 2;

    int numberOfDisksOnNNStick = 0;
    if (rest1X == nextNextStickLocation) numberOfDisksOnNNStick++;
    if (rest2X == nextNextStickLocation) numberOfDisksOnNNStick++;

    p("number of disks on next stick: " + numberOfDisksOnNNStick);

    return numberOfDisksOnNNStick;
  }

  public int widthOnNStick(JButton current, JButton rest1, JButton rest2)
  {
    int widthOnNextStick = 0;

    int nextStickLocation = nStickLocation(current, rest1, rest2);

    Point p1 = rest1.getLocation();
    int rest1X = p1.x + rest1.getWidth() / 2;
    Point p2 = rest2.getLocation();
    int rest2X = p2.x + rest2.getWidth() / 2;

    if (rest1X == nextStickLocation) widthOnNextStick = rest1.getWidth();
    if (rest2X == nextStickLocation) widthOnNextStick = rest2.getWidth();

    return widthOnNextStick;
  }

  public int widthOnNNStick(JButton current, JButton rest1, JButton rest2)
  {
    int widthOnNextNextStick = 0;

    int nextNextStickLocation = nnStickLocation(current, rest1, rest2);

    Point p1 = rest1.getLocation();
    int rest1X = p1.x + rest1.getWidth() / 2;
    Point p2 = rest2.getLocation();
    int rest2X = p2.x + rest2.getWidth() / 2;

    if (rest1X == nextNextStickLocation) widthOnNextNextStick = rest1.getWidth();
    if (rest2X == nextNextStickLocation) widthOnNextNextStick = rest2.getWidth();

    return widthOnNextNextStick;
  }

  public void move(JButton current, JButton rest1, JButton rest2)
  {
    int currentDiskWidth = current.getWidth();

    int nextStickLocation = nStickLocation(current, rest1, rest2);
    int diskNumberOnNStick = nStickHasDisks(current, rest1, rest2);
    int diskNumberOnNNStick = nnStickHasDisks(current, rest1, rest2);
    int nextWidth = widthOnNStick(current, rest1, rest2);
    int nextNextWidth = widthOnNNStick(current, rest1, rest2);

    int nX = 0;
    int nnX = 0;
    int nY = 0;
    int nnY = 0;

    int w = current.getWidth();

    if (currentDiskWidth == 60)
    {
      if (nextStickLocation == 105)
      {
        if (diskNumberOnNStick == 0) { nX = 75; nY = 170; }
        if (diskNumberOnNStick == 1) { nX = 75; nY = 145; }
        if (diskNumberOnNStick == 2) { nX = 75; nY = 120; }
      }
      if (nextStickLocation == 215)
      {
        if (diskNumberOnNStick == 0) { nX = 185; nY = 170; }
        if (diskNumberOnNStick == 1) { nX = 185; nY = 145; }
        if (diskNumberOnNStick == 2) { nX = 185; nY = 120; }
      }
      if (nextStickLocation == 325)
      {
        if (diskNumberOnNStick == 0) { nX = 295; nY = 170; }
        if (diskNumberOnNStick == 1) { nX = 295; nY = 145; }
        if (diskNumberOnNStick == 2) { nX = 295; nY = 120; }
      }
      current.setLocation(nX, nY);
    }

    if (currentDiskWidth == 80)
    {
      if (nextStickLocation == 105)
      {
        if (diskNumberOnNStick == 0)
        {
          nX = 65;
          nY = 170;
          current.setLocation(nX, nY);
        }
        if (diskNumberOnNStick == 1)
        {
          if (nextWidth > w)
          {
            nX = 65;
            nY = 145;
            current.setLocation(nX, nY);
          }
          else if (nextNextWidth > w)
          {
            nnX = 175;
            nnY = 145;
            current.setLocation(nnX, nnY);
          }
          else if (diskNumberOnNNStick == 0)
          {
            nnX = 175;
            nnY = 170;
            current.setLocation(nnX, nnY);
          }
        }
        if (diskNumberOnNStick == 2)
        {
          nnX = 175;
          nnY = 170;
          current.setLocation(nnX, nnY);
        }
      }
      if (nextStickLocation == 215) {
        if (diskNumberOnNStick == 0)
        {
          nX = 175;
          nY = 170;
          current.setLocation(nX, nY);
        }
        if (diskNumberOnNStick == 1)
        {
          if (nextWidth > w)
          {
            nX = 175;
            nY = 145;
            current.setLocation(nX, nY);
          }
          else if (nextNextWidth > w)
          {
            nnX = 285;
            nnY = 145;
            current.setLocation(nnX, nnY);
          }
          else if (diskNumberOnNNStick == 0)
          {
            nnX = 285;
            nnY = 170;
            current.setLocation(nnX, nnY);
          }
        }
        if (diskNumberOnNStick == 2)
        {
          nnX = 285;
          nnY = 170;
          current.setLocation(nnX, nnY);
        }
      }
      if (nextStickLocation == 325)
      {
        if (diskNumberOnNStick == 0)
        {
          nX = 285;
          nY = 170;
          current.setLocation(nX, nY);
        }
        if (diskNumberOnNStick == 1)
        {
          if (nextWidth > w)
          {
            nX = 285;
            nY = 145;
            current.setLocation(nX, nY);
          }
          else if (nextNextWidth > w)
          {
            nnX = 65;
            nnY = 145;
            current.setLocation(nnX, nnY);
          }
          else if (diskNumberOnNNStick == 0)
          {
            nnX = 65;
            nnY = 170;
            current.setLocation(nnX, nnY);
          }
        }
        if (diskNumberOnNStick == 2)
        {
          nnX = 65;
          nnY = 170;
          current.setLocation(nnX, nnY);
        }
      }
    }

    if (currentDiskWidth == 100)
    {
      if (nextStickLocation == 105)
      {
        if (diskNumberOnNStick == 0)
        {
          nX = 55;
          nY = 170;
          current.setLocation(nX, nY);
        }
        if (diskNumberOnNStick == 2)
        {
          nnX = 165;
          nnY = 170;
          current.setLocation(nnX, nnY);
        }
      }
      if (nextStickLocation == 215)
      {
        if (diskNumberOnNStick == 0)
        {
          nX = 165;
          nY = 170;
          current.setLocation(nX, nY);
        }
        if (diskNumberOnNStick == 2)
        {
          nnX = 275;
          nnY = 170;
          current.setLocation(nnX, nnY);
        }
      }
      if (nextStickLocation == 325)
      {
        if (diskNumberOnNStick == 0)
        {
          nX = 275;
          nY = 170;
          current.setLocation(nX, nY);
        }
        if (diskNumberOnNStick == 2)
        {
          nnX = 55;
          nnY = 170;
          current.setLocation(nnX, nnY);
        }
      }
    }
  }

  public boolean finished(JButton b1, JButton b2, JButton b3)
  {
    Point pb1 = b1.getLocation();
    Point pb2 = b2.getLocation();
    Point pb3 = b3.getLocation();

    int B1X = pb1.x + b1.getWidth() / 2;
    //int B1Y = pb1.y;

    int B2X = pb2.x + b2.getWidth() / 2;
    //int B2Y = pb2.y;

    int B3X = pb3.x + b3.getWidth() / 2;
    //int B3Y = pb3.y;

    return (B1X == B2X) && (B2X == B3X) && (B3X == 325);
  }
}