import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class MyActionListener
  implements ActionListener
{
  private JButton B1;
  private JButton B2;
  private JButton B3;
  private JButton Base;
  private JLabel Count;
  private Methods M;

  public MyActionListener(JButton b1, JButton b2, JButton b3, JLabel count, JButton base)
  {
    B1 = b1;
    B2 = b2;
    B3 = b3;
    Count = count;
    Base = base;
    M = new Methods();
  }

  public void actionPerformed(ActionEvent e) {
    Object event = e.getSource();
    if (event == B1)
    {
      M.p("\nB1 is clicked");
      M.move(B1, B2, B3);
      Count.setText("" + (Integer.parseInt(Count.getText()) + 1));
      M.p("B1 can be moved");
    }

    if (event == B2)
    {
      M.p("\nB2 is clicked");
      if (M.canBeMoved(B2, B1, B3))
      {
        M.p("B2 can be moved");
        M.move(B2, B1, B3);
        Count.setText("" + (Integer.parseInt(Count.getText()) + 1));
      } else {
        M.p("B2 can not be moved");
      }
    }
    if (event == B3)
    {
      M.p("\nB3 is clicked");
      if (M.canBeMoved(B3, B1, B2))
      {
        M.p("B3 can be moved");
        M.move(B3, B1, B2);
        Count.setText("" + (Integer.parseInt(Count.getText()) + 1));
      } else {
        M.p("B3 can not be moved");
      }
    }
    if (M.finished(B1, B2, B3))
    {
      Base.setText("Congratulations:)");
    }
  }
}